<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/auth')->group(function () {
    Route::prefix('/student')->group(function () {
        Route::post('/login', 'AuthController@studentLogin');

        Route::middleware('auth:student')->group(function () {
            Route::get('/me', 'AuthController@studentMe');
            Route::post('/logout', 'AuthController@studentLogout');
        });
    });

    Route::prefix('/staff')->group(function () {
        Route::post('/login', 'AuthController@staffLogin');

        Route::middleware('auth:staff')->group(function () {
            Route::get('/me', 'AuthController@staffMe');
            Route::post('/logout', 'AuthController@staffLogout');
        });
    });
});

Route::middleware('auth:staff')->group(function () {
    Route::prefix('/students')->group(function () {
        Route::get('/', 'StudentController@index');
        Route::post('/', 'StudentController@store');

        Route::prefix('/{student}')->group(function () {
            Route::get('/', 'StudentController@show');
            Route::put('/', 'StudentController@update');
            Route::delete('/', 'StudentController@destroy');
        });
    });

    Route::get('/careers', 'CareerController@index');
});


// REST:
// students

// GET  (listar)    /students
// POST (guardar)   /students
// GET  (ver)       /students/{id}
// PUT  (editar)    /students/{id}
// DELETE (borrar)  /students/{id}

// RPC:

// /reenviar-notificacion-inscrito

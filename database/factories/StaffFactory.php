<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'second_last_name' => $faker->lastName,
        'rfc' => $faker->lexify('??????'),
        'password' => bcrypt($faker->password)
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Career;
use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'second_last_name' => $faker->lastName,
        'control_number' => $faker->numerify('########'),
        'password' => bcrypt($faker->password),
        'career_id' => Career::inRandomOrder()->first()
    ];
});

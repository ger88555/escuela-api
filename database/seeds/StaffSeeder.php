<?php

use Illuminate\Database\Seeder;
use App\Staff;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(Staff::class)->create([
            'rfc' => 'admin',
            'password' => bcrypt('password')
        ]);
        $admin->assignRole('Servicios Escolares');

        $nonAdmin = factory(Staff::class)->create([
            'rfc' => 'non-admin',
            'password' => bcrypt('password')
        ]);
        
        factory(Staff::class, 10)->create();
    }
}

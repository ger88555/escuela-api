<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\{Role, Permission};

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schoolServices = Role::create(['name' => 'Servicios Escolares', 'guard_name' => 'staff']);

        Permission::create(['name' => 'crear estudiantes', 'guard_name' => 'staff']);
        Permission::create(['name' => 'listar estudiantes', 'guard_name' => 'staff']);
        Permission::create(['name' => 'editar estudiantes', 'guard_name' => 'staff']);
        Permission::create(['name' => 'eliminar estudiantes', 'guard_name' => 'staff']);

        $schoolServices->givePermissionTo([
            'crear estudiantes',
            'listar estudiantes',
            'editar estudiantes',
            'eliminar estudiantes'
        ]);
    }
}

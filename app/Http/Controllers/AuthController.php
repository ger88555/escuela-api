<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\{StaffResource, StudentLogin, StudentResource};

class AuthController extends Controller
{
    public function staffLogin(Request $request)
    {
        $credenciales = $request->only('rfc', 'password');

        if ($token = Auth::guard('staff')->attempt($credenciales)) {
            return response()->json(['success' => true, 'token' => $token]);
        } else {
            return response()->json(['success' => false], 401);
        }
    }

    public function studentLogin(Request $request)
    {
        $credenciales = $request->only('control_number', 'password');

        if ($token = Auth::guard('student')->attempt($credenciales)) {
            return response()->json(['success' => true, 'token' => $token]);
        } else {
            return response()->json(['success' => false], 401);
        }
    }

    public function staffMe()
    {
        try {
            return response()->json([
                'success' => true,
                'user' => new StaffResource( Auth::guard('staff')->user() )
            ]);
        } catch (\Throwable $th) {
            return response()->json(['successs' => false], 500);
        }
    }

    public function studentMe()
    {
        try {
            return response()->json([
                'success' => true,
                'user' => new StudentResource(Auth::guard('student')->user())
            ]);
        } catch (\Throwable $th) {
            return response()->json(['successs' => false], 500);
        }
    }

    public function staffLogout()
    {
        try {
            Auth::guard('staff')->logout();

            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }

    public function studentLogout()
    {
        try {
            Auth::guard('student')->logout();

            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }
}

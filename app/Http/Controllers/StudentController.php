<?php

namespace App\Http\Controllers;

use App\Student;
use App\Http\Resources\StudentResource;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $students = Student::with('career')->get();
    
            return response()->json(['success' => true, 'data' => StudentResource::collection($students)]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request)
    {
        try {
            $newStudent = Student::create($request->validated());
    
            return response()->json(['success' => true, 'data' => new StudentResource($newStudent->load('career'))]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        try {
            return response()->json(['success' => true, 'data' => new StudentResource($student->load('career'))]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        try {
            $student->update($request->validated());
    
            return response()->json(['success' => true, 'data' => new StudentResource($student->load('career'))]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        try {
            $student->delete();
    
            return response()->json(['success' => true]);
        } catch (\Throwable $th) {
            return response()->json(['success' => false], 500);
        }
    }
}
